# Protractor

Detect drawn gestures in Godot using the Protractor gesture recognition
algorithm.

**Protractor** is a library that provides an implementation of the
[Protractor gesture recognition algorithm][proc] for the Swift programming
language, along with an additional extension library for using it in the
Godot game engine.

## Getting started

### Using the algorithm library

Add the following in your `dependencies`:

```swift
    .package(url: "https://github.com/Indexing-Your-Heart/core-dependencies/Protractor", branch: "root")
```

In your target, define the dependency on `Protractor`:

```swift
.target(...,
        dependencies: [
            .package(name: "Protractor", package: "Protractor")
        ])
```

### Using the Godot extension

**Required Tools**  
- An Apple Silicon Mac running macOS 13 Ventura or later
- Xcode 15 or later, or Swift 5.9 or later

Clone the repository using `git clone`, then run `swift build` to build
the dylib file.

In your Godot project, create a `bin` directory and write a new
`Protractor.gdextension` file:

```
[configuration]
entry_symbol = "libprotractor_entry_point"
compatibility_minimum = 4.1

[libraries]
macos.debug = "res://bin/libProtractorGodotInterop.dylib"
macos.release = "res://bin/libProtractorGodotInterop.dylib"
```

Copy the built dylib file to your `bin` directory, then open Godot.

[proc]: https://static.googleusercontent.com/media/research.google.com/en//pubs/archive/36269.pdf
