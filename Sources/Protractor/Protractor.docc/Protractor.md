# ``Protractor``

Recognize drawn gestures quickly and easily.

## Overview

The Protractor library provides the tools for creating gesture recognition systems using the Protractor gesture
recognition algorithm. Its corrolary library, **ProtractorGodotInterop**, can be used with the Godot game engine to add
a gesture recognition tool inside any game.

## Topics

### Templates
- <doc:Using-Templates>
- ``ProtractorTemplate``
- ``ProtractorTemplateCodable``
- ``ProtractorTemplateRepresentable``

### Recognition

- ``ProtractorRecognizer``
- ``ProtractorRecognitionDelegate``
