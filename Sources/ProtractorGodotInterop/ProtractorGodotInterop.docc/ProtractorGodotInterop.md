# ``ProtractorGodotInterop``

Recognize drawn gestures easily and quickly within Godot.

## Overview

The ProtractorGodotInterop library builds on the Protractor library to add extra functionality for recognizing drawn
gestures in the Godot game engine through GDExtension.


