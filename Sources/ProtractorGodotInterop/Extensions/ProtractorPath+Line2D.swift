//
//  ProtractorPath+Line2D.swift
//  Indexing Your Heart
//
//  Created by Marquis Kurt on 5/25/23.
//


import Foundation
import Protractor
import SwiftGodot

public extension ProtractorPath {
    init(line: Line2D) {
        self.init(points: line.points.map(ProtractorPoint.init))
    }
}
