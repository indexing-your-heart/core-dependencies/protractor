//
//  ProtractorPoint+Vector2.swift
//  Indexing Your Heart
//
//  Created by Marquis Kurt on 5/25/23.
//


import Foundation
import Protractor
import SwiftGodot

public extension ProtractorPoint {
    /// A Vector2 that represents this point.
    var vector: Vector2 {
        Vector2(x: Float(x), y: Float(y))
    }

    init(from vector: Vector2) {
        self.init(x: Double(vector.x), y: Double(vector.y))
    }
}
