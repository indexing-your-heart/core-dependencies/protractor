//
//  Protractor.swift
//  Indexing Your Heart
//
//  Created by Marquis Kurt on 5/19/23.
//


import Logging
import SwiftGodot

@GodotMain
class LibProtractor: GodotExtensionDelegate {
    static var logger = Logger(label: "godotengine.swiftgodot.libprotractor")
    func extensionWillInitialize() {
        LoggingSystem.bootstrap(GodotLogger.init)
    }
    func extensionDidInitialize(at level: GDExtension.InitializationLevel) {
        register(type: ProtractorDrawer.self)
        GD.pushWarning(
            """
            ProtractorGodotInterop is deprecated and will be removed in a future update.

            Future puzzles should utilize the Ashashat library.
            """
        )
    }
}
